import { 
    Pipe, 
    PipeTransform 
 } from '@angular/core';  
 
 @Pipe ({ 
    name: 'even' 
 }) 
 
 export class EvenPipe implements PipeTransform { 
    transform(value: string, index: number): string {
       return index % 2 == 0 ? value : "";
    } 
 } 