import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes-test',
  templateUrl: './pipes-test.component.html',
  styleUrls: ['./pipes-test.component.scss']
})
export class PipesTestComponent implements OnInit {
  strings: Array<string> = [
    'I',
    'was',
    'lOwErCaSe',
    'AND',
    'now',
    'I\'m',
    'uppercase'
  ];

  myDate: number = Date.now();
  constructor() { }

  ngOnInit(): void {
  }

}
