import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvenPipe } from 'src/custom-pipes/even.pipe';
import { PipesTestComponent } from 'src/components/pipes-test/pipes-test.component';
import { HighlightElementDirective } from 'src/custom-directives/highlight.directive';

@NgModule({
  declarations: [
    PipesTestComponent,
    EvenPipe,
    HighlightElementDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PipesTestComponent
  ]
})
export class PipesTestModule { }
