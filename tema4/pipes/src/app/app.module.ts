import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PipesTestModule } from 'src/modules/pipes-test/pipes-test.module';

@NgModule({
  declarations: [
    AppComponent  
  ],
  imports: [
    BrowserModule,
    PipesTestModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
