import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[highlightElement]'
})

export class HighlightElementDirective {

    constructor(private elRef: ElementRef) {
        elRef.nativeElement.style.background = 'red';
        elRef.nativeElement.style.border = '1px solid black';
    }

}