import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from 'src/components/app/app.component';
import { PersonModule } from '../person/person.module';
import { PrincipalModule } from '../principal/principal.module';
import { TeacherModule } from '../teacher/teacher.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PersonModule,
    PrincipalModule,
    TeacherModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
