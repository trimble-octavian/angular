import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrincipalComponent } from 'src/components/principal/principal.component';
import { TeacherModule } from '../teacher/teacher.module';


@NgModule({
  declarations: [ 
    PrincipalComponent
  ],
  imports: [
    TeacherModule,
    CommonModule
  ],
  exports: [
    PrincipalComponent
  ]
})
export class PrincipalModule { }
