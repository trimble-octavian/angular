import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeacherComponent } from 'src/components/teacher/teacher.component';
import { PersonModule } from '../person/person.module';

@NgModule({
  declarations: [
    TeacherComponent
  ],
  imports: [
    PersonModule,
    CommonModule
  ],
  exports: [
    TeacherComponent,
    PersonModule
  ]
})

export class TeacherModule { }
