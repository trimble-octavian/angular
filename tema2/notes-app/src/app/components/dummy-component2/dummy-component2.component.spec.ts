import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyComponent2 } from './dummy-component2.component';

describe('DummyComponent2', () => {
  let component: DummyComponent2;
  let fixture: ComponentFixture<DummyComponent2>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DummyComponent2 ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyComponent2);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
