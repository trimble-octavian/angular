import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dummy-component2',
  templateUrl: './dummy-component2.component.html',
  styleUrls: ['./dummy-component2.component.scss']
})
export class DummyComponent2 implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
