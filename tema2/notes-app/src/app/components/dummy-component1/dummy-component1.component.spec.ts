import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyComponent1 } from './dummy-component1.component';

describe('DummyComponent1', () => {
  let component: DummyComponent1;
  let fixture: ComponentFixture<DummyComponent1>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DummyComponent1 ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyComponent1);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
