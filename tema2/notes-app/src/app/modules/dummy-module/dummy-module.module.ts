import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DummyComponent1 } from 'src/app/components/dummy-component1/dummy-component1.component';
import { DummyComponent2 } from 'src/app/components/dummy-component2/dummy-component2.component';


@NgModule({
  declarations: [
    DummyComponent1,
    DummyComponent2
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DummyComponent1,
    DummyComponent2
  ]
})
export class DummyModule { }
