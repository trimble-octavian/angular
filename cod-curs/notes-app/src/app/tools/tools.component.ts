import { Component, OnInit } from '@angular/core';
import { Colors, GetEnumLength } from '../auxiliar/color-enum';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }
}
