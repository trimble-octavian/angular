import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Note } from '../note/note.interface';

@Injectable({
  providedIn: 'root'
})

export class NoteService {
  readonly baseUrl= "https://localhost:4200";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private httpClient: HttpClient) {

  }

  getFilteredNotes(categId: string): Observable<Note[]> {
    return this.httpClient
      .get<Note[]>(
        this.baseUrl + `/notes`,
        this.httpOptions
      )
      .pipe(
        map((notes) => notes.filter((note) => note.categoryId === categId))
      );
  }


  serviceCall() {
    console.log("Note service was called");
  }

  getNotes():Observable<Note[]> {
    return this.httpClient.get<Note[]>(this.baseUrl+`/notes`, this.httpOptions);
  }

  getNote(noteId: string):Observable<Note> {
    return this.httpClient.get<Note>(this.baseUrl+`/notes/`+noteId, this.httpOptions);
  }

  addNote(note: Note){
  return this.httpClient.post(this.baseUrl+"/note", note, this.httpOptions);
  }

  updateNote(note: Note, noteId: string){
    return this.httpClient.put(this.baseUrl+`/notes/`+noteId, note, this.httpOptions);
  }

  deleteNote(noteId: string) {    
    return this.httpClient.delete(this.baseUrl+"/notes/" + noteId);
  }

  // getNotes() {
  //   return this.notes;
  // }
  
  // getFilteredNotes(argCategoryId: string) {
  //   return this.notes.filter((nota) => {
  //     return nota.categoryId === argCategoryId
  //   });
  //   // return this.notes.filter(nota => nota.categoryId === argCategoryId);
  // }

  // getNotesByTitleSearched(titleTerm: string) {
  //   return this.notes.filter(nota => nota.title.includes(titleTerm));
  // }

  // getNotesByDescriptionSearched(descriptionTerm: string) {
  //   return this.notes.filter(nota => nota.description.includes(descriptionTerm));
  // }

  // addNote(deAdaugat: Note) {
  //   this.notes.push(deAdaugat);
  // }
}
