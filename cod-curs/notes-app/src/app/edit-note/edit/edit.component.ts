import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, RouteConfigLoadEnd, Router } from '@angular/router';
import { Category } from 'src/app/auxiliar/category';
import { Note } from 'src/app/note/note.interface';
import { FilterService } from 'src/app/services/filter.service';
import { NoteService } from 'src/app/services/note.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  noteId: string;

  note: Note;
  editNoteForm: FormGroup;
  categories: Category[];

  constructor(private noteService: NoteService,
    private filterService: FilterService,
    private route: ActivatedRoute,
    private router: Router) {
      this.route.params.subscribe(args => this.noteId = args['id']);
      this.noteService.getNote(this.noteId).subscribe(note => this.note = note);
    }

  ngOnInit(): void {
    this.categories = this.filterService.getCategories();
    this.initForm();
    this.editNoteForm.patchValue({
      'id': this.note.id,
      'title': this.note.title,
      'description': this.note.description,
      'categoryId': this.note.categoryId
    })
  }

  onSubmit() {
    const note:Note = {
      'id': this.editNoteForm.controls['id'].value,
      'title': this.editNoteForm.controls['title'].value,
      'description': this.editNoteForm.controls['description'].value,
      'categoryId': this.editNoteForm.controls['categoryId'].value
    };
    this.noteService.updateNote(note, this.noteId).subscribe();
    this.router.navigate(['']);
  }

  private initForm() {
    this.editNoteForm = new FormGroup({
      'id': new FormControl(null),
      'title': new FormControl(null, Validators.required),
      'description': new FormControl(null, Validators.required),
      'categoryId': new FormControl(null, Validators.required),
    })
  }

}
