import { Component, Input, OnInit } from '@angular/core';
import { NoteService } from '../services/note.service';
import { Note } from './note.interface';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {
  notes: Note[] = [];
  inputTitle: string;
  inputDescription: string;
  inputDescriptionAndTitle: string;
  @Input() selectedCategoryId:string = "";

  constructor(private noteService: NoteService) {
   }

  ngOnInit(): void {
    this.noteService.getNotes().subscribe((notes: Note[]) => {
      this.notes = notes;
    });
    // this.notes = this.noteService.getNotes();
  }

  ngOnChanges(): void {
    this.noteService.getFilteredNotes(this.selectedCategoryId).subscribe((notes: Note[]) => {
      this.notes = notes;
    });
  }

  getSearchTitle(inputTitleParam:string) {
    // this.inputTitle = inputTitleParam;
  }

  useTitleSearch() {
    // this.notes = this.noteService.getNotesByTitleSearched(this.inputTitle);
  }

  getSearchDescription(inputDescriptionParam:string) {
    // this.inputDescription = inputDescriptionParam;
  }

  getSearchDescriptionAndTitle(inputDescriptionAndTitleParam:string) {
    this.inputDescriptionAndTitle = inputDescriptionAndTitleParam;
  }

  useDescriptionSearch() {
    // this.notes = this.noteService.getNotesByDescriptionSearched(this.inputDescription);
  }

  deleteNote(noteId: string) {    
    this.noteService.deleteNote(noteId).subscribe();
    this.noteService.getNotes().subscribe((notes: Note[]) => {
      this.notes = notes;
    });
  }

  useDescriptionAndTitleSearch() {
    this.notes = this.noteService.getNotesByTitleAndDescriptionSearched(this.inputDescriptionAndTitle);
  }
}
