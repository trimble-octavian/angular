import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NoteService } from '../services/note.service';
import { Note } from '../note/note.interface';
import { Category } from '../auxiliar/category';
import { FilterService } from '../services/filter.service';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent implements OnInit {
  // liniile astea doua sunt pentru template driven approach - se comenteaza restul, pentru a folosi template driven approach
  // title: string = "";
  // description: string = "";
  addNoteForm: FormGroup;
  categories: Category[];
  

  constructor(private noteService: NoteService,
    private filterService: FilterService,
    private router: Router) {

  }

  ngOnInit(): void {
    this.initForm();
    this.categories = this.filterService.getCategories();
  }

  onSubmit() {
    const note:Note = {
      'id': this.addNoteForm.controls['id'].value,
      'title': this.addNoteForm.controls['title'].value,
      'description': this.addNoteForm.controls['description'].value,
      'categoryId': this.addNoteForm.controls['categoryId'].value
    };
    this.noteService.addNote(note).subscribe();
    this.router.navigate(['']);
  }

  private initForm() {
    this.addNoteForm = new FormGroup({
      'id': new FormControl(null),
      'title': new FormControl(null, Validators.required),
      'description': new FormControl(null, Validators.required),
      'categoryId': new FormControl(null, Validators.required),
    })
  }

}
