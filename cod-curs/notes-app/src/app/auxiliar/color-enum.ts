export enum Colors {
    'red',
    'green',
    'yellow',
    'pink',
    'purple',
    'white'
}

export function GetEnumLength(enumeration: any): number {
    return Object.keys(Colors).length/2;
}