import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { filter } from 'rxjs';
import { Category } from '../auxiliar/category';
import { FilterService } from '../services/filter.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  @Output() emitSelectedFilter = new EventEmitter<string>();

  categories: Category[];

  constructor(private filterService: FilterService) { }

  ngOnInit(): void {
    this.categories = this.filterService.getCategories();
  }

  selectFilter(categoryId: string)
  {
    this.emitSelectedFilter.emit(categoryId);
  }
}
