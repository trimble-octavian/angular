import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output() searchTitleChanged: EventEmitter<string> = new EventEmitter();
  @Output() searchDescriptionChanged: EventEmitter<string> = new EventEmitter();
  @Output() searchDescriptionAndTitleChanged: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  sendSearchTitle(title: string) {
    this.searchTitleChanged.emit(title);    
  }

  sendSearchDescription(description: string) {
    this.searchDescriptionChanged.emit(description);    
  }

  sendSearchDescriptionAndTitle(word: string) {
    this.searchDescriptionAndTitleChanged.emit(word);    
  }
}
