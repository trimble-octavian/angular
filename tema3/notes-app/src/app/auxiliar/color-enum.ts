export enum Colors {
    'red',
    'green',
    'yellow',
    'pink',
    'purple',
    'white'
}

export function getEnumLength(enumeration: any): number {
    return Object.keys(Colors).length/2;
}