import { Component, OnInit } from '@angular/core';
import { Colors, getEnumLength } from '../auxiliar/color-enum';
import getRandomInt from '../auxiliar/getRandomInt';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit {
  title: string = "Add note";
  titleColor: string = "red";
  noteContent: string = "";
  titleBg: string = "white";

  colors = Colors;

  constructor() { }

  ngOnInit(): void {
  }

  selectColor() {
    this.titleBg = this.colors[getRandomInt(0, getEnumLength(this.colors))];
  }

  changeNoteContent() {
    this.noteContent = "Note content modificat de pe buton"
  }
}
