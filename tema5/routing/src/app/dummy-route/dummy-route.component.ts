import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, Params } from '@angular/router';

@Component({
  selector: 'app-dummy-route',
  templateUrl: './dummy-route.component.html',
  styleUrls: ['./dummy-route.component.scss']
})
export class DummyRouteComponent implements OnInit {
  paramsData: Params;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.paramsData = params;
    });
  }

}
