import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DummyRouteComponent } from './dummy-route.component';

@NgModule({
  declarations: [
    DummyRouteComponent
  ],
  imports: [
    CommonModule,
    DummyRouteModule
  ]
})
export class DummyRouteModule { }
