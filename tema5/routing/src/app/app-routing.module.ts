import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DummyRouteComponent } from './dummy-route/dummy-route.component';

const routes: Routes = [
  { path: 'dummy-route/:dummy-id', component: DummyRouteComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
